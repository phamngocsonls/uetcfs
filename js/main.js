(function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input2').each(function() {
        $(this).on('blur', function() {
            if($(this).val().trim() != '') {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        })    
    })

    /*==================================================================
    [ Validate ]*/
    var confession = $('.validate-input textarea[name="entry.670048132"]');

    $('.validate-form').on('submit',function() {
        var check = true;
        if($(confession).val().trim() == '') {
            showValidate(confession);
            check = false;
        }
        if(check == true) {
            $('.step1').hide();
            $('.step2').show();
        }
        return check;
    });

    $('.validate-form .input2').each(function() {
        $(this).focus(function() {
            hideValidate(this);
        });
    });

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);